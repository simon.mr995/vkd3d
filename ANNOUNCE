The Wine team is proud to announce that release 1.9 of vkd3d, the Direct3D to
Vulkan translation library, is now available.

This release contains improvements that are listed in the release notes below.
The main highlights are:

  - Yet more improvements to the HLSL compiler.
  - A new utility to inspect the contents of DXBC blobs.
  - Miscellaneous bug fixes.

The source is available from the following location:

  https://dl.winehq.org/vkd3d/source/vkd3d-1.9.tar.xz

The current source can also be pulled directly from the git repository:

  https://gitlab.winehq.org/wine/vkd3d.git

Vkd3d is available thanks to the work of multiple people. See the file AUTHORS
for the complete list.

----------------------------------------------------------------

What's new in vkd3d 1.9
=======================


*** libvkd3d

- Copying between depth/stencil and colour formats in
  ID3D12GraphicsCommandList::CopyResource() is supported.
- The ID3D12Fence1 interface is supported.


*** libvkd3d-shader

- vkd3d_shader_scan() supports retrieving descriptor information for `d3dbc'
  shaders. This is one of the requirements for eventual SPIR-V generation from
  `d3dbc' sources.

- New features for the HLSL source type:
  - Support for the following intrinsic functions:
    - clip()
    - ddx_coarse() and ddy_coarse()
    - ddx_fine() and ddy_fine()
    - tex1D(), tex2D(), texCUBE(), and tex3D()
  - Constant folding support for more expression types. In particular:
    - comparison operators
    - floating-point min() and max()
    - logical `and' and `or'
    - dot products
    - square roots
    - logarithms
  - Support for multi-sample texture object declarations without explicit
    sample counts in shader model 4.1 and later shaders.
  - Support for using constant expressions as sample counts in multi-sample
    texture object declarations.
  - Support for variable initialisers using variables declared earlier in the
    same declaration list. E.g., `float a = 1, b = a, c = b + 1;'.
  - The GetDimensions() texture object method is implemented.
  - Matrix swizzles are implemented.
  - Parser support for if-statement attributes like `[branch]' and
    `[flatten]'.
  - Support for the `inline' function modifier.

- Previously, vkd3d_shader_compile() would in some cases return VKD3D_OK
  despite compilation failing when targeting legacy Direct3D bytecode. These
  cases have been fixed.

- Various HLSL preprocessor fixes for edge cases related to stringification.

- SPIR-V target support for the `linear noperspective centroid' input
  interpolation mode.

- New interfaces:
  - The vkd3d_shader_scan_signature_info structure extends the
    vkd3d_shader_compile_info structure, and can be used to retrieve
    descriptions of `dxbc-tpf' and `d3dbc' shader inputs and outputs.
  - vkd3d_shader_free_scan_signature_info() is used to free
    vkd3d_shader_scan_signature_info structures.
  - The VKD3D_SHADER_COMPILE_OPTION_PACK_MATRIX_ORDER compile option can be
    used to specify the default matrix packing order for HLSL sources.
  - The vkd3d_shader_varying_map_info structure extends the
    vkd3d_shader_compile_info structure, and can be used to specify a mapping
    between the outputs of a shader stage and the inputs of the next shader
    stage.
  - vkd3d_shader_build_varying_map() is used to build a mapping between the
    outputs of a shader stage and the inputs of the next shader stage.
  - The VKD3D_SHADER_DESCRIPTOR_INFO_FLAG_RAW_BUFFER flag returned as part of
    the vkd3d_shader_descriptor_info structure indicates the descriptor refers
    to a byte-addressed (`raw') buffer resource.


*** vkd3d-compiler

- The `--matrix-storage-order' option can used to specify the default matrix
  storage order for HLSL sources.


*** vkd3d-dxbc

- vkd3d-dxbc is a new utility that can be used to inspect the contents of DXBC
  blobs.
